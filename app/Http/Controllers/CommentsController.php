<?php

namespace App\Http\Controllers;
use App\Http\Requests\CommentsRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
        public function store(Post $post,CommentsRequest $request): RedirectResponse{
        $data=$request->validated();
        $comment=new Comment();
        $comment->post_id=$post->id;
        $comment->text=$data['text'];
        $comment->user_id=auth()->user()->id;
        $comment->save();
        return back();
    }

    public function destroy(Request $request):RedirectResponse{
        $comment=Comment::find($request->comment_id);
        $comment->delete();
        return back();
    }
}
