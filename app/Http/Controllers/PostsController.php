<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Like;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Post;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        Cache::store('redis')->remember('showPosts',now()->addMinutes(10),function (){
            return 'cache index';
        });
        $posts = Post::withCount('comments')->withCount('likes')->paginate(2);

       /* $posts= "select * from posts order by `id` asc limit 2 offset 0";*/
//            $posts=DB::select('select  posts.*, users.name as user_name, count(comments.id) as comments_count,
//            likeable_like_counters.count as likeCount
//                from posts
//                    inner join users on posts.user_id=users.id
//                    inner join comments on posts.id=comments.commentable_id
//                    right join likeable_like_counters on posts.id=likeable_like_counters.likeable_id
//                group by posts.id ,likeable_like_counters.count
//                limit 3 offset 0
//       '
//        );

        return view('blog.index', [
            'posts' => $posts
        ]);
    }

    /**
     * @return Application|Factory|View
     */

    public function newestPosts()
    {
        Cache::store('redis')->remember('newestPosts',now()->addMinutes(10),function (){
            return 'cache newestPosts';
        });
        Cache::get('newestPosts');
        $newestPosts = Post::orderBy('updated_at', 'DESC')->withCount('comments')->withCount('likes')->paginate(2);

        return view('blog.index')
            ->with('posts', $newestPosts);
    }

    public function mostLikedPosts() {
        $mostLikedPosts = Post::withCount('comments')->withCount('likes')->with('user')->orderBy('likes_count', 'DESC')->paginate(2);

        return view('blog.index')
            ->with('posts', $mostLikedPosts);
    }
    public function mostViewedPosts() {
        $mostViewedPosts = Post::withCount('comments')->withCount('likes')->with('user')->orderBy('views','DESC')->paginate(2);

        return view('blog.index')
            ->with('posts', $mostViewedPosts);
    }
    public function postsByUsers() {
        $postsByUsers = Post::withCount('comments')->withCount('likes')->orderBy('user_id','ASC')->paginate(2);

        return view('blog.index')
            ->with('posts', $postsByUsers);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpg,png,jpeg|max:5048'
        ]);
        if ($request->image) {
       $newImageName = uniqid() . '-' . $request->title . '.' . $request->image->extension();

        $request->image->move(public_path('images'), $newImageName);;
        } else {
            $newImageName = '';
        }

        Post::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'slug' => SlugService::createSlug(Post::class, 'slug', $request->title),
            'image_path' => $newImageName,
            'user_id' => auth()->user()->id
        ]);
        return redirect('/blog')
            ->with('message', 'Your post has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return Application|Factory|View
     */

    public function show($slug ,Post $post)
    {
        Cache::store('redis')->remember('showPosts',now()->addMinutes(10),function (){
            return 'cache show Posts';
        });
        Cache::get('showPosts');
        $countViews = Post::where('slug', $slug);
        $countViews->increment('views');

        return view('blog.show',[
            'post'=>Post::where('slug', $slug)->first(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param string $slug
     * @return Application|Factory|View
     */
    public function edit($slug)
    {
        return view('blog.edit')
            ->with('post', Post::where('slug', $slug)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $slug
     * @return Application|Redirector|RedirectResponse
     */
    public function update(Request $request, $slug)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        Post::where('slug', $slug)
            ->update([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'slug' => SlugService::createSlug(Post::class, 'slug', $request->title),
                'user_id' => auth()->user()->id
            ]);

        return redirect('/blog')
            ->with('message', 'Your post has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $slug
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy($slug)
    {
        $post = Post::where('slug', $slug);
        $post->delete();

        return redirect('/blog')
            ->with('message', 'Your post has been deleted!');
    }
    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function search(Request $request)
    {
        $request->validate([
            'query' => 'required',

        ]);
        $query = $request->input('query');
        $posts = Post::where('title', 'like', "%$query%")->orWhere('description', 'like', "%$query%")->get();
        return view('blog.search', compact('posts'));
    }

    public function showCategory($category_slug)
    {
        $category=Category::where('slug',$category_slug)->first();
    return view('blog.categories',[
                    'category'=>$category
    ]);
    }
}
