<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Post;
class Search extends Component
{
    public $query='';
    public $results;
    protected $queryString=['query'=>['except'=>'']];

    public function mount()
    {
        $this->reset();
    }
    public function render()
    {
           $this->results=Post::where('title','like',"%{$this->query}%")->get();

        return view('livewire.search');
    }
}
