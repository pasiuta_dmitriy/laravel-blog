<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;

    protected $table = 'posts_likes';

    protected $fillable = [
        'user_id',
        'post_id',

    ];

    /**
     * Get all of the posts that are assigned this like.
     */
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'posts_likes');
    }
    public function totalLikes(){
        return $this->hasMany('Like')->whereUserId($this->author_id)->count();
    }
}
