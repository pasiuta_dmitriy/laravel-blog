@extends('layouts.app')
@section('content')
    <div class="  w-4/5 m-auto text-left">
        <div class="py-15 ">
            <h1 class=" font-extrabold  text-4xl">
                Create Post :
            </h1>
        </div>
    </div>

    @if($errors->any())
        <div class="w-4/5 m-auto">
            <ul>
                @foreach($errors->all() as $error)
                    <li class="w-1/5 mb-4 text-gray-50 bg-red-700 rounded 2x py-4">
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
        @endif
    <div class="w-4/5 m-auto pt-20">
        <form
            action="/blog"
            method="POST"
            enctype="multipart/form-data">
            @csrf
            <div>
                <h2 class="font-extrabold  text-4xl">Title: </h2>
                <input
                    type="text"
                    name="title"

                    class="mb-4 bg-transparent block-b-2 w-full h-20 text-4xl outline-none border-solid border-4 border-light-blue-500">
            </div>
            <h2 class="font-extrabold  text-4xl">Description: </h2>
            <textarea
                type="text"
                name="description"
                placeholder="Description..."
                class=" bg-transparent  block border-b-2 w-full h-60 text-4xl outline-none border-solid border-4 border-light-blue-500">
            </textarea>
            <h2 class="font-extrabold  text-4xl">Category: </h2>

            <select name="category_id"> <!--Supplement an id here instead of using 'name'-->

                @foreach($categories as $category)

                        <option value="{{$category->id}}"><a href="google.com">{{$category->slug}}</a></option>
                        @endforeach
                    </select>


            <div class="bg-grey-lighter pt-15">
                <label class=" w-44 flex flex-col items-center px-2 py-3 bg-white-rounded-lg shadow-lg tracking-wide
               uppercase border border-blue cursor-pointer">
                <span class="mt-2 text-base leading-normal">
                    Select a file
                </span>
                    <input type="file"
                           name="image"
                           class="hidden">
                </label>
            </div>
            <button
                type="submit"
                class="uppercase mt-15 bg-blue-500 text-gray-100 text-lg font-extrabold py-4 px-8 rounded-3xl">
                Submit Post
            </button>
        </form>
    </div>

@endsection
