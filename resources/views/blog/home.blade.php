@extends('layouts.app')

@section('content')

    <div class="flex h-screen">
    <div class="mt-8 m-auto py-15  ">
        <h1 class="text-4xl">
            Welcome to my Blog Application

        </h1>
    </br>
       <h2 class="pl-8"> you can go to this <a href="blog"><span class="text-blue-800 label label-danger">page</span></a> to try all the features of the application</h2>
    </div>

    </div>
@endsection
