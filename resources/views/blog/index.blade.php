@extends('layouts.app')

@section('content')
    <div class="w-4/5 m-auto text-center">
    <ul class="float-left mt-8 pl-12">
        <h2 class="-mt-8 border-gray-600  font-bold">Filters:</h2>

        <li><form
            class="mt-4 uppercase bg-transparent text-black text-xs font-extrabold font-extrabold rounded-3xl  "
            method="GET" action="{{url('/newestPosts')}}">
            <button
                class="">
                Newest Posts
            </button>

        </form>
        </li>
        <li class="mt-4 -mr-4">
        <form
            class="uppercase bg-transparent text-black text-xs font-extrabold font-extrabold rounded-3xl  "
            method="GET" action="{{url('/mostLikedPosts')}}">
            <button
                class="">
                Most Liked Posts
            </button>

        </form>
        </li>
        <li class="mt-4 -mr-6">
        <form
            class=" uppercase bg-transparent text-black text-xs font-extrabold font-extrabold rounded-3xl  "
            method="GET" action="{{url('/mostViewedPosts')}}">
            <button
                class="">
                Most Viewed Posts
            </button>

        </form>
        </li>
        <li class="mt-4 mr-9">
            <form
                class="uppercase bg-transparent text-black text-xs font-extrabold font-extrabold rounded-3xl  "
                method="GET" action="{{url('/postsByUsers')}}">
                <button
                    class="">
                    By User
                </button>

            </form>
        </li>
    </ul>

            <h2 class="float-left ml-20 border-gray-600  font-bold">Categories:</h2>

            <ul class="mt-4 float-left">
                @foreach($categories as $category)
                    <li class="-ml-24 mt-3  bg-transparent text-black text-xs  rounded-3xl "><a href="{{route('showCategory',$category->slug)}}">{{$category->title}}</a> </li>
                @endforeach
            </ul>
        <div class="py-15 border-b border-gray-600">
            <h1 class="text-6xl">
                Blog Posts
            </h1>

        </div>
    </div>

    @if (session()->has('message'))
        <div class="w-4/5 m-auto mt-10 pl-2">
            <p class="w-2/6 mb-4 text-gray-50 bg-green-500 rounded-2xl py-4">
                {{ session()->get('message') }}
            </p>
        </div>
    @endif

    @if (Auth::check())
        <div class="pt-15 w-4/5 m-auto">
            <a
                href="/blog/create"
                class="bg-blue-500 uppercase bg-transparent text-gray-100 text-xs font-extrabold py-3 px-5 rounded-3xl">
                Create post
            </a>
        </div>
    @endif

    @foreach ($posts as $post)
        <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-15 border-b border-gray-600">
            <div class="relative   overflow-hidden">
                @if($post->image_path)
                    <h3 class="bg-green-700 text-lg font-extrabold py-2 px-3 rounded-2xl absolute text-white text-xs text-center leading-4">{{$post->category->title}}</h3>
                    <img src="{{ asset('images/' . $post->image_path) }}" alt="" class="object-cover">
                @else
                    <img src="{{ asset('images/kotik.jpg' . $post->image_path) }}">
                    {{--no image available--}}
                @endif

                <h6 class="mt-8 text-sm font-bold py-1 px-2 border border-blue-400 shadow-sm rounded-md hover:shadow-md">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 inline-block" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M2 5a2 2 0 012-2h7a2 2 0 012 2v4a2 2 0 01-2 2H9l-3 3v-3H4a2 2 0 01-2-2V5z" />
                        <path d="M15 7v2a4 4 0 01-4 4H9.828l-1.766 1.767c.28.149.599.233.938.233h2l3 3v-3h2a2 2 0 002-2V9a2 2 0 00-2-2h-1z" />
                    </svg>
                    <span>: {{ $post->comments_count}}</span>
                </h6>
                <h6 class="mt-8 text-sm font-bold py-1 px-2 border border-blue-400 shadow-sm rounded-md hover:shadow-md">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 inline-block" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M10 12a2 2 0 100-4 2 2 0 000 4z" />
                        <path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd" />
                    </svg>
                    <span>: {{ $post->views}}</span>
                </h6>


            </div>
            <div>
                <h2 class="text-gray-700 font-bold text-5xl pb-4">
                    {{ $post->title }}
                </h2>


                <span class="text-gray-500">
                By <span class="font-bold italic text-gray-800">{{ $post->user->name }}</span>, Created on {{ date('jS M Y', strtotime($post->created_at)) }}
            </span>
                <p class="pb-6  text-xl text-gray-700 pt-8 pb-10 leading-8 font-light">
                    {{ $post->description }}
                </p>
                <a href="/blog/{{ $post->slug }}"
                   class="uppercase bg-green-700 text-gray-100 text-lg font-extrabold py-3 px-5 rounded-2xl">
                    Keep Reading
                </a>

                <div>
                    <div class="mt-8">
                        <form action="{{ route('post.like', $post->id) }}"
                              method="post">
                            @csrf
                            <button
                                class="mb-4 px-4 py-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 inline-block" viewBox="0 2 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                                </svg>
                                <span> {{ $post->likes_count}}</span>

                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach



    {{$posts->links()}}
@endsection
