@extends('layouts.app')
@section('content')

    <div class="w-4/5 m-auto text-left">
        <div class="py-15 ">
            <h1 class="text-6xl">
                {{$post->title}}
            </h1>

        </div>
    </div>

    <div class="w-4/5 m-auto pt-20">
        <span class="text-gray-500">
            By <span class="font-bold italic text-gray-800">{{$post->user->name}}</span>,
            Created on {{ date('jS M Y', strtotime($post->created_at)) }}
        </span>
        <h3 class="text-gray-500 pt-4">Category: <span class="font-bold italic text-gray-800">{{$post->category->title}}</span></h3>
        @if (isset(Auth::user()->id) && Auth::user()->id == $post->user_id)
            <span class="">
                    <a
                        href="/blog/{{ $post->slug }}/edit"
                        class="italic hover:text-gray-900  m-80 pb-1 bg-transparent hover:bg-blue-400 text-gray-700  font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded ">
                        Edit Post
                    </a>
                </span>

            <span class="float-right">
                     <form
                         action="/blog/{{ $post->slug }}"
                         method="POST">
                        @csrf
                         @method('delete')

                        <button
                            class=" -m-8 pr-3 italic hover:text-red-800  bg-transparent hover:bg-red-400 text-gray-700  font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded pb-1 border-b-2"
                            type="submit">
                            Delete Post
                        </button>

                    </form>
                </span>



        @endif
        <p class="text-xl text-gray-900 pt-4 pb-40 leading-8 font-light">
            {{$post->description}}
        </p>
    <div>

        <h2 class="mt-6 text-4xl leading-10 tracking-tight font-bold text-gray-900 text-center">Comments</h2>
<div>

            <form action="/blog/{{$post->id}}/comments" method="POST" class="md-0">
                @csrf
                <label for="author" class="mt-6 text-sm font-medium text-gray-700"></label>
                <textarea name="text" class="mt-1 py-2 px-3 block w-full border border-gray-400 rounded-md shadow-sm"></textarea>
                @if($errors->any())
                    <div class="mt-6">
                        <ul class="bg-red-100 px-4 py-5 rounded-md">
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <button type="submit" class="m-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">Add comment</button>
            </form>

        </div>

       @include('livewire.comments')

       {{-- <div class="mt-6">
        @foreach($post->comments as $comment)

           <div class="flex">
               <div class="flex flex-col justify-center">
                   <p class="font-bold italic text-gray-800">{{ $comment->user->name }}</p>
                   <p class="text-gray-600">{{$comment->created_at->diffForHumans()}}</p>
               </div>
           </div>
               <div class="mt-3">
                   <p>{{$comment->text}}</p>
               <form action="/blog/{{$post->slug}}/comments/delete/{{$comment->id}}" method="POST" class="mb-4 mt-3">
                   @csrf
                   @method('delete')
                   <button type="submit" class="text-sm py-1 px-2 border border-blue-400 shadow-sm rounded-md hover:shadow-md">Delete</button>
               </form>
           </div>

            @endforeach
        </div>--}}

    </div>
    </div>

@endsection
