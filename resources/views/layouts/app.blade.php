<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @livewireStyles
</head>
</html>
<body class="bg-gray-100 h-screen antialiased leading-none font-sans">
<div id="app">
    <header class="bg-gray-800 py-6">
        <div class="container mx-auto flex justify-between items-center px-6">
            <div>
                <a href="{{ url('/') }}" class=" ml-24 text-lg  text-gray-100 no-underline">
                    {{-- {{ config('app.name', 'Laravel') }}--}}Laravel Blog
                </a>
            </div>
            <div class="space-x-4 text-gray-300 text-sm sm:text-base ">
                <div class="-mt-2 float-left flex items-center justify-center ">
                    {{--@livewire('search')--}}
                    <livewire:search/>
                   {{-- <form
                        class=" mr-6  uppercase bg-transparent text-black text-xs font-extrabold font-extrabold rounded-3xl  "
                        method="GET" action="{{url('/search')}}">
                        <input type="text" class=" px-4 py-2 w-80 rounded-xl " name="query" type="search"
                               placeholder="Search...">

                        <button class=" p-2.5 px-8 text-white bg-blue-700 rounded-3xl text-xs font-extrabold ">
                            Search
                        </button>
                    </form>--}}
                </div>

                <a class="no-underline hover:underline" href="/">Home</a>
                <a class="no-underline hover:underline" href="/blog">Blog</a>
                @guest
                    <a class="no-underline hover:underline" href="{{ route('login') }}">{{ __('Login') }}</a>
                    @if (Route::has('register'))
                        <a class="no-underline hover:underline" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                @else
                    <span>{{ Auth::user()->name }}</span>

                    <a href="{{ route('logout') }}"
                       class="no-underline hover:underline"
                       onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                        {{ csrf_field() }}
                    </form>
                    @endguest
                    </form>
            </div>
        </div>
    </header>
    <div>
        @yield('content')
        <script src="js/app.js"></script>
    </div>
@livewireScripts
</div>
</body>
</html>
