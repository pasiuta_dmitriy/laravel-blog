<div>
    <div class="mt-6">
        @foreach($post->comments as $comment)

            <div class="flex">
                <div class="flex flex-col justify-center">

                    <p class="mt-4 font-bold italic text-gray-800">{{ $comment->user->name }}</p>
                    <p class="text-gray-600">{{$comment->created_at->diffForHumans()}}</p>
                </div>

            </div>

            <div class="mt-3">

                <p>{{$comment->text}}</p>
                @if (isset(Auth::user()->id) && Auth::user()->id == $comment->user_id)
                <form action="/blog/{{$post->slug}}/comments/delete/{{$comment->id}}" method="POST" class="mb-4 mt-3">
                    @csrf
                    @method('delete')
                    <button type="submit" class="text-sm py-1 px-2 border border-blue-400 shadow-sm rounded-md hover:shadow-md">Delete</button>
                </form>
            </div>
            @endif
        @endforeach
{{--            <a wire:click="load" class="btn btn-primary">Load more...</a>--}}
         {{--   {{ $post->comments->links("livewire.tailwind")}}--}}
    </div>
</div>
