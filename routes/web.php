<?php

use App\Http\Controllers\LikeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\CommentsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [\App\Http\Controllers\PagesController::class, 'index'])->name('home');
Route::resource('/blog', \App\Http\Controllers\PostsController::class);
Route::get('/livewire', [\App\Http\Controllers\PostsController::class,'livewire'])->name('livewire');
Auth::routes();
Route::get('/search', '\App\Http\Controllers\PostsController@search')->name('search');
Route::get('/newestPosts', '\App\Http\Controllers\PostsController@newestPosts')->name('newestPosts');
Route::get('/mostLikedPosts', '\App\Http\Controllers\PostsController@mostLikedPosts')->name('mostLikedPosts');
Route::get('/mostViewedPosts', '\App\Http\Controllers\PostsController@mostViewedPosts')->name('mostViewedPosts');
Route::get('/postsByUsers', '\App\Http\Controllers\PostsController@postsByUsers')->name('postsByUsers');
Route::get('/{category}','App\Http\Controllers\PostsController@showCategory')->name('showCategory');
Route::post('blog/like/{id}', [LikeController::class,'likePost'])->name('post.like');
Route::post('/blog/{post}/comments',[CommentsController::class,'store']);
Route::delete('/blog/{post}/comments/delete/{comment_id}',[CommentsController::class,'destroy']);

